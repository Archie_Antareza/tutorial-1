from django.shortcuts import render

mhs_name = 'Muhammad Archie Antareza'
pengalaman_1 = 'Professionalism division staff of Society of Exploration Geophysicists (SEG) ITS SC'
pengalaman1_desc = 'In charge of holding knowledge upgrading events (Short Courses, Seminars, Guest Lectures, Workshops, and Open Discussions) regarding exploration geophysics. Have successfully held one Short Course so far. Working with a team of 7 people.'
pengalaman_2 = 'Liason Officer of Geoscience Atmosphere (Geosphere) 2019, Surabaya'
pengalaman2_desc = 'Accepted as a part of the Liason Officer (LO) team for Geosphere 2019, a geoscience big event held by Himpunan Mahasiswa Teknik Geofisika ITS (HMTG ITS). The LO team actively receives skill upgrading before working on the D-day. Working with a team of 31 people.'
# Create your views here.
def my_experience(request):
    response = {'name': mhs_name,'peng1': pengalaman_1, 'peng2': pengalaman_2, 'desc1': pengalaman1_desc, 'desc2': pengalaman2_desc}
    return render(request, 'my_experience.html', response)