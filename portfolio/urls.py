from django.conf import settings
from django.conf.urls.static import static
from django.urls import re_path
from .views import portfolio

urlpatterns = [
    re_path(r'^portfolio/$', portfolio, name='portfolio'),
]