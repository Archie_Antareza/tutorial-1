from django.shortcuts import render

desc1='This website is actually my first project.'
# Create your views here.
def portfolio(request):
    response = {'desc1': desc1}
    return render(request, 'portfolio.html', response)