from django.conf import settings
from django.conf.urls.static import static
from django.urls import re_path
from .views import index
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
]

app_name='lab_1'