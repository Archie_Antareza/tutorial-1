from django.shortcuts import render
from datetime import datetime, date
from django.http import HttpResponseRedirect
# Enter your name here
mhs_name = 'Muhammad Archie Antareza' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2001,1,23) #TODO Implement this, format (Year, Month, Date)
npm = '03411840000007' # TODO Implement this
ttl = 'Cirebon, 23 January 2001'
asal = 'Surabaya'
kuliah='Geophysical Engineering (Undergraduate)'
univ ='Institut Teknologi Sepuluh Nopember (ITS)'
ans = 'Archie is a Geophysical Engineering student with insatiable thirst for knowledge and new experiences. First introduced to the world of programming through a lecture using MATLAB, he has since developed an interest in computer science and data science. Realizing the imminent rapid technological advancements of Industrial Revolution 4.0, he strives to promote the collaboration of data science and geophysics.'
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'ttl': ttl, 'asal': asal, 'kuliah': kuliah, 'univ': univ, 'ans': ans}
    return render(request, 'index_lab1.html', response)
    
def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0